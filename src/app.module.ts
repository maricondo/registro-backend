import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { typeOrmConfigAsync } from './database/typeorm.config';
import { RegistroNotasModule } from './modules/registro-notas/registro-notas.module';
import { OficinasModule } from './modules/oficinas/oficinas.module';
import { AsignarRegistrosModule } from './modules/asignar-registros/asignar-registros.module';
import { TipoDocumentoModule } from './modules/tipo-documento/tipo-documento.module';
import { configValidationSchema } from './config.schema';


@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath:[`.env`],
      validationSchema: configValidationSchema

    }),
    TypeOrmModule.forRootAsync(typeOrmConfigAsync),
    TypeOrmModule.forFeature([]),
    RegistroNotasModule,
    OficinasModule,
    AsignarRegistrosModule,
    TipoDocumentoModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number | string;

  constructor (private readonly configService:ConfigService){
    AppModule.port = this.configService.get('PORT');
  }

}
