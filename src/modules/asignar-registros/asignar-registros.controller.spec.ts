import { Test, TestingModule } from '@nestjs/testing';
import { AsignarRegistrosController } from './asignar-registros.controller';
import { AsignarRegistrosService } from './asignar-registros.service';

describe('AsignarRegistrosController', () => {
  let controller: AsignarRegistrosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AsignarRegistrosController],
      providers: [AsignarRegistrosService],
    }).compile();

    controller = module.get<AsignarRegistrosController>(AsignarRegistrosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
