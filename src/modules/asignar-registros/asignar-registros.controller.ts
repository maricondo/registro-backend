import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes, ValidationPipe, Res, HttpStatus } from '@nestjs/common';
import { AsignarRegistrosService } from './asignar-registros.service';
import { CreateAsignarRegistroDto } from './dto/create-asignar-registro.dto';
import { UpdateAsignarRegistroDto } from './dto/update-asignar-registro.dto';

@Controller('asignar-registros')
export class AsignarRegistrosController {
  constructor(private readonly asignarRegistrosService: AsignarRegistrosService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  create(
    @Body() createAsignarRegistroDto: CreateAsignarRegistroDto,
    @Res() response) {
      this.asignarRegistrosService.createAsignacionRegistro(createAsignarRegistroDto)
      .then((mensaje) => {
        response.status(HttpStatus.CREATED).json(mensaje);
      })
      .catch(() => {
        response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ mensaje: 'Error en la creacion del registro'})
      });
      
  }


  //@Get(':id')
  getAll(@Param('id') id:number, @Res() response){
    return this.asignarRegistrosService.registroAsignaciones(id)
      .then((result) => {
        response.status(HttpStatus.OK).json({
          message: 'Asignaciones obtenidas correctamente.',
          result,
          status: true
        });
      })
      .catch(e => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({
            message: 'Ocurrio un error, revise e intente de nuevo',
            status: false
          })
      });
  }

  @Get(':numeroRegistro')
  async obtenerAsignaciones(
    @Param('numeroRegistro') numeroRegistro:string, 
    @Res() response){
    try{
      const res = await this.asignarRegistrosService.obtenerAsignacionesPorRegistro(numeroRegistro);
      response.status(HttpStatus.OK).json(res);
    } catch(e) {
      response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ mensaje: 'Error en la obtencion del registro' });
    }
  }

}
