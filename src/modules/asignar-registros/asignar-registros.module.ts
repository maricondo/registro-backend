import { Module } from '@nestjs/common';
import { AsignarRegistrosService } from './asignar-registros.service';
import { AsignarRegistrosController } from './asignar-registros.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AsignarRegistro } from './entities/asignar-registro.entity';
import { AsignarRegistroRepository } from './entities/asignar-registro.repository';
import { RegistroNotaRepository } from '../registro-notas/entities/registro-nota.repository';


@Module({
  imports: [
    TypeOrmModule.forFeature([AsignarRegistro, AsignarRegistroRepository, RegistroNotaRepository]),
  ],
  controllers: [AsignarRegistrosController],
  providers: [AsignarRegistrosService]
})
export class AsignarRegistrosModule {}
