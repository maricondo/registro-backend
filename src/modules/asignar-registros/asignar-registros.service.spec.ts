import { Test, TestingModule } from '@nestjs/testing';
import { AsignarRegistrosService } from './asignar-registros.service';

describe('AsignarRegistrosService', () => {
  let service: AsignarRegistrosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AsignarRegistrosService],
    }).compile();

    service = module.get<AsignarRegistrosService>(AsignarRegistrosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
