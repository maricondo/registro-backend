import { Injectable, Logger } from '@nestjs/common';
import { RegistroNotaRepository } from '../registro-notas/entities/registro-nota.repository';
import { CreateAsignarRegistroDto } from './dto/create-asignar-registro.dto';
import { AsignarRegistro } from './entities/asignar-registro.entity';
import { AsignarRegistroRepository } from './entities/asignar-registro.repository';

@Injectable()
export class AsignarRegistrosService {

  private readonly logger = new Logger(AsignarRegistrosService.name);

  constructor(
    //@InjectRepository(AsignarRegistro)
    //private asignarRegistroRepository: Repository<AsignarRegistro>,
    private asignarRegistroRepository: AsignarRegistroRepository,
    private registroRepository:RegistroNotaRepository,
  ){}

  async createAsignacionRegistro(
    body: CreateAsignarRegistroDto 
  ): Promise<RespuestaDto>{
    const instancia = this.asignarRegistroRepository.create(body);
    try {
      const asignacionRealizada = await this.asignarRegistroRepository.save(body);
      let respuesta: RespuestaDto = {
        ok: true,
        mensaje: '',
        mensajeUI: 'registro realizado correctamente',
        respuesta: asignacionRealizada,
      };
      return respuesta;
    } catch (error) {
      this.logger.log(error);
      let respuesta: RespuestaDto = {
        ok: false,
        mensaje: '',
        mensajeUI: 'Ocurrio un error durante el registro',
        respuesta: null,
      };
      return respuesta;
    }
  }

  async registroAsignaciones(id): Promise<AsignarRegistro[]>{
    return await this.asignarRegistroRepository.find({
      select: ["id",
              "idRegistro",
              "idOficina",
            "fechaAsignacion",
            "fechaRespuesta",
            "proveido"],
      where: {idRegistro: id },
      order: {id: "DESC"}
    });

  }

  async obtenerAsignacionesPorRegistro(
    numeroRegistro:string
  ): Promise <RespuestaDto>{
    try{
      const registro = await this.registroRepository.findOne({
        where: {"numeroRegistro":numeroRegistro}
      });
      if(registro){
        const query = await this.asignarRegistroRepository
          .createQueryBuilder('asignarRegistro')
          .innerJoinAndSelect('asignarRegistro.registroNota', 'registroNota')
          .innerJoinAndSelect('asignarRegistro.oficinaAsignacion','oficina')
          .select([
            'registroNota.id',
            'registroNota.numeroRegistro',
            'asignarRegistro.id',
            'asignarRegistro.idRegistro',
            'asignarRegistro.proveido',
            'asignarRegistro.idOficina',
            'asignarRegistro.fechaAsignacion',
            'asignarRegistro.fechaRespuesta',
            'oficina.nombreOficina'
          ])
          .where('asignarRegistro.idRegistro = :idRegistro', {
            idRegistro:registro.id
          })
        const asignaciones = await query.getMany();

        let respuesta: RespuestaDto = {
          ok: true,
          mensaje: 'La consulta devuelve resultados',
          mensajeUI: 'La consulta devuelve resultados',
          respuesta: asignaciones,
        };
        return respuesta;
      } else {
        let respuesta: RespuestaDto = {
          ok: false,
          mensaje: 'No se encontro el registro',
          mensajeUI: 'No se encontro el registro',
          respuesta: null,
        };
        return respuesta;
      }
    } catch (error){
      this.logger.log(error);
      let respuesta: RespuestaDto = {
        ok: false,
        mensaje: 'Error: ' + error,
        mensajeUI: 'ocurrio un error durante la consulta',
        respuesta: null,
      };
      return respuesta;
    }
  }
}