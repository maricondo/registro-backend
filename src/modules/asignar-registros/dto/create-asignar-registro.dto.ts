import { IsNotEmpty, Length } from "class-validator";

export class CreateAsignarRegistroDto {

    @IsNotEmpty({message: 'Actividad principal es requerida'})
    idRegistro: number;
    @IsNotEmpty({message: 'Actividad principal es requerida'})
    idOficina: number;
    @IsNotEmpty({message: '$property es obligatorio'})
    fechaAsignacion: Date
    @IsNotEmpty({message: '$property es obligatorio'})
    fechaRespuesta: Date;
    @Length(2,150,{message: '$property debe tener entre $constraint1 a $constraint2 caracteres'})
    proveido: string;
    
}
