import { PartialType } from '@nestjs/mapped-types';
import { CreateAsignarRegistroDto } from './create-asignar-registro.dto';

export class UpdateAsignarRegistroDto extends PartialType(CreateAsignarRegistroDto) {}
