import { Oficina } from "src/modules/oficinas/entities/oficina.entity";
import { RegistroNota } from "src/modules/registro-notas/entities/registro-nota.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity({ schema: 'public', name: 'asignaciones'})
export class AsignarRegistro {

    @PrimaryGeneratedColumn('increment', { name: 'asignacion_id' })
    id: number;

    @Column({ type: 'int4', name:'id_registro', nullable: false })
    idRegistro: number;

    @Column({ type: 'int4', name:'id_oficina', nullable: false })
    idOficina: number;

    @Column({ type: 'date', name: 'fecha_derivacion', nullable: true })
    fechaAsignacion: Date;

    @Column({ type: 'date', name: 'fecha_respuesta', nullable: true })
    fechaRespuesta: Date;

    @Column({ type: 'varchar', name: 'proveido', nullable: true })
    proveido: string;

    @ManyToOne(() => Oficina, (oficina) => oficina, {
        cascade: false,
        nullable: false,
        eager: true,
    })
    @JoinColumn({ name: 'id_oficina'})
    oficinaAsignacion: Oficina;

    @ManyToOne(type => RegistroNota, registro => registro.asignaciones)
    @JoinColumn({name: 'id_registro'})
    registroNota:RegistroNota;

}
