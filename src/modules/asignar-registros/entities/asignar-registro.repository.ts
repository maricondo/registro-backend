import { EntityRepository, Repository } from "typeorm";
import { AsignarRegistro } from "./asignar-registro.entity";

@EntityRepository(AsignarRegistro)
export class AsignarRegistroRepository extends Repository<AsignarRegistro>{}