import { Expose } from "class-transformer";
import { IsNotEmpty } from "class-validator";

export class CreateOficinaDto {

    @Expose()
    @IsNotEmpty({message:'Nombre de oficina es requerido'})
    nombreOficina: string;
    @Expose()
    @IsNotEmpty({message:'Descripcion es dato requerido'})
    descripcion: string;
    @Expose()
    @IsNotEmpty({message:'Responsable es dato requerido'})
    responsable: string;




}
