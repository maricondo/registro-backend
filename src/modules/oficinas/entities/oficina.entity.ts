import { AsignarRegistro } from "src/modules/asignar-registros/entities/asignar-registro.entity";
import { RegistroNota } from "src/modules/registro-notas/entities/registro-nota.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({ schema: 'public', name:'oficinas'})
export class Oficina {
    @PrimaryGeneratedColumn('increment', { name: 'oficina_id' })
    id: number;

    @Column({ type: 'varchar', name: 'nombre_oficina', nullable: false })
    nombreOficina: string;

    @Column({ type: 'varchar', name: 'descripcion', nullable: true })
    descripcion: string;

    @Column({ type: 'varchar', name: 'responsable', nullable: true })
    responsable: string;

    @OneToMany(() => RegistroNota, registroNota => registroNota.oficinaRemitente)
    registroNota:RegistroNota;

    @OneToMany(() => AsignarRegistro, asignarRegistro => asignarRegistro.oficinaAsignacion)
    asignacionRegistro: AsignarRegistro;
}
