import { Controller, Get, UsePipes, Post, Body, Patch, Param, Delete, Res, HttpStatus, ValidationPipe, Put, Query } from '@nestjs/common';
import { OficinasService } from './oficinas.service';
import { CreateOficinaDto } from './dto/create-oficina.dto';

@Controller('oficinas')
export class OficinasController {
  constructor(private readonly oficinasService: OficinasService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  create(
    @Body() createOficinaDto: CreateOficinaDto, @Res() response) {
      return this.oficinasService.create(createOficinaDto)
      .then((mensaje) => {  
        response.status(HttpStatus.CREATED).json(mensaje);
      })
      .catch(() => {
        response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({mensaje: 'Error en la creacion de registro'})
      });
  }

  @Get()
  getAll(@Query('page') pagina, @Res() response){
    return this.oficinasService.getAll(pagina)
      .then((result) => {
        response.status(HttpStatus.OK).json({
            message: 'Oficinas obtenidas correctamente...',
            result,
            status: true
        });
      })
    .catch(e => {
      response
        .status(HttpStatus.FORBIDDEN)
        .json({
          message: 'Ocurrio un error, revise e intente nuevamente',
          status: false
        })
    });
  }

  /*
  @Put(':id')
  @UsePipes(new ValidationPipe())
  update(
    @Body() createOficinaDto: CreateOficinaDto, 
    @Res() response,
    @Param('id') idNum,){
      this.oficinasService
      .update(idNum, createOficinaDto)
      .then((res) => {
        response.status(HttpStatus.OK).json(res);
      })
      .catch(() => {
        response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({mensaje:'error en la eliminacion del registro'})
      } )


    }*/


}
