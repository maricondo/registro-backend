import { EntityRepository, Repository } from "typeorm";
import { Oficina } from "./entities/oficina.entity";


@EntityRepository(Oficina)
export class OficinasRepository extends Repository<Oficina>{}