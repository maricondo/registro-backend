import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { last } from 'rxjs';
import { Repository } from 'typeorm';
import { CreateOficinaDto } from './dto/create-oficina.dto';
import { UpdateOficinaDto } from './dto/update-oficina.dto';
import { Oficina } from './entities/oficina.entity';

@Injectable()
export class OficinasService {

  private readonly logger = new Logger (OficinasService.name) 
  constructor (
    @InjectRepository(Oficina)
    private oficinasRepository: Repository<Oficina>,
    //private oficinasRepository:OficinasRepository
  ){}
  
  async create(createOficinaDto: CreateOficinaDto): Promise<RespuestaDto> {
    const nuevaOficina = this.oficinasRepository.create(createOficinaDto);
    try {
      const oficinaCreado = await this.oficinasRepository.save(nuevaOficina);
      if(oficinaCreado){
        let respuesta: RespuestaDto = {
          ok: true,
          mensaje: 'Registro creado exitosamente',
          mensajeUI: 'Registro realizado exitosamente',
          respuesta: oficinaCreado,
        };
        return respuesta;
      }
    } catch (error) {
      let respuesta: RespuestaDto = {
        ok: false,
        mensaje: 'ocurrio un error durante el registro',
        mensajeUI: 'ocurrio un error durante el registro',
        respuesta: null,
      }; 
      return respuesta;
    }
  }

  async getAll(pagina){
    if(pagina){
      const take = 10;
      const page = pagina;
      const [oficinas, total] = await this.oficinasRepository.findAndCount({
          select: ["nombreOficina", "id"],
          take, skip: (page -1)* take
      });
      return{
        data: oficinas, meta: {total, page, last_page: Math.ceil(total / take )}
      } 
    }else {
      return await this.oficinasRepository.find({
        select: ["nombreOficina", "id"],
      }
      );
  }
}



  
}
