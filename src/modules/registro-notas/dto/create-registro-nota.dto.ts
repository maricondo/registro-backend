import { Exclude, Expose } from "class-transformer";
@Exclude()
export class CreateRegistroNotaDto {
    @Expose()
    id: number; 
    @Expose()
    numeroRegistro: string; 
    @Expose()
    fechaIngreso: Date;
    @Expose()
    remitente: string;
    @Expose()
    unidadRemitente: string;
    @Expose()
    descripcion: string;
    @Expose()
    numeroFojas: number;
    @Expose()
    tipoDocumento: string;
}
