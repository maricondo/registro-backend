import { Exclude, Expose } from 'class-transformer';
import {  IsNotEmpty, IsNumber, IsString, Length} from 'class-validator';


export class CreateRegistroDto {
    
    @IsNotEmpty({message: 'Numero de registro es requerido'})
    numeroRegistro: string; 
    @IsNotEmpty({message: '$property es obligatorio'})
    fechaIngreso?: Date;
    @IsNotEmpty({message: 'El dato del remitente es requerido'})
    remitente: string;
    @IsNotEmpty({message: 'Actividad principal es requerida'})
    idOficina: number;
    @IsNotEmpty({message: 'La descripcion del registro es requerido'})
    descripcion: string;
    @IsNotEmpty({message: 'El Id de oficina es requerido'})
    numeroFojas: string;
    @IsNotEmpty({message: 'El tipo de documento es requerido'})
    tipoDocumento: string;
}