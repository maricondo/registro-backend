import { PartialType } from '@nestjs/mapped-types';
import { CreateRegistroNotaDto } from './create-registro-nota.dto';

export class UpdateRegistroNotaDto extends PartialType(CreateRegistroNotaDto) {}
