import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
} from 'typeorm';

@Entity({ schema: 'public', name: 'clasificadores' })
export class Estado {
  @PrimaryGeneratedColumn('increment', { name: 'clasificador_id' })
  id: number;

  @Column({ nullable: false })
  codigo: number;

  @Column({ type: 'varchar', name: 'tipo_clasificador', nullable: false, })
  tipoClasificador: string;

  @Column({ type: 'varchar', nullable: false })
  descripcion: string;

  @Column({ type: 'boolean', name: 'estado_registro', nullable: false })
  estadoRegistro: boolean;
  
}
