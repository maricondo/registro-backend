import { AsignarRegistro } from 'src/modules/asignar-registros/entities/asignar-registro.entity';
import { Oficina } from 'src/modules/oficinas/entities/oficina.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Estado } from './estado.entity';

@Entity({ schema: 'public', name:'registros'})
@Unique(['numeroRegistro'])
export class RegistroNota {
    @PrimaryGeneratedColumn('increment', { name: 'registro_id' })
    id: number;

    @Column({ type: 'varchar', name: 'numero_registro', nullable: false })
    numeroRegistro: string;

    @Column({ type: 'date', name: 'fecha_ingreso', nullable: true })
    fechaIngreso: Date;

    @Column({ type: 'varchar', nullable: true })
    remitente: string;

    @Column({ type: 'int4', name:'id_oficina', nullable: false })
    idOficina: number;

    @Column({ type: 'varchar', nullable: false })
    descripcion: string;

    @Column({name:'numero_fojas', nullable: true })
    numeroFojas: string;

    @Column({ type: 'varchar', name:'tipo_documento', nullable: false })
    tipoDocumento: string;

    @Column({ type: 'int4', name:'estado_id', nullable: false})
    estadoId: number;

    @Column({ default: false, select: false })
    estado_registro: boolean;

    @ManyToOne(() => Oficina, (oficina)=> oficina.registroNota, {
        cascade: false,
        nullable: false,
        eager: true,
    })
    @JoinColumn({ name: 'id_oficina'})
    oficinaRemitente: Oficina;

    @ManyToOne(() => Estado, {
        cascade: false,
        nullable: false,
        eager: true,
      })
    @JoinColumn({ name: 'estado_id' })
    estado: Estado;

    @OneToMany(type => AsignarRegistro, asignacion => asignacion.registroNota)
    asignaciones: AsignarRegistro[];
}