import { EntityRepository, Repository } from "typeorm";
import { RegistroNota } from "./registro-nota.entity";

@EntityRepository(RegistroNota)
export class RegistroNotaRepository extends Repository<RegistroNota>{}