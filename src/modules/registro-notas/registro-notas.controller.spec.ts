import { Test, TestingModule } from '@nestjs/testing';
import { RegistroNotasController } from './registro-notas.controller';
import { RegistroNotasService } from './registro-notas.service';

describe('RegistroNotasController', () => {
  let controller: RegistroNotasController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RegistroNotasController],
      providers: [RegistroNotasService],
    }).compile();

    controller = module.get<RegistroNotasController>(RegistroNotasController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
