import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Query, UsePipes, ValidationPipe, Put } from '@nestjs/common';
import { response } from 'express';
import { DriverPackageNotInstalledError } from 'typeorm';
import { CreateRegistroDto } from './dto/create-registro.dto';

import { RegistroNotasService } from './registro-notas.service';

@Controller('registros')
export class RegistroNotasController {
  constructor(private readonly registroNotasService: RegistroNotasService) {}

//@Get()
getAll(@Query('page') pagina, @Res() response){
  return this.registroNotasService.getAll(pagina)
      .then((result) => {
        response.status(HttpStatus.OK).json({
          message: 'Registros obtenidos correctamente.',
                    result,
                    status: true
      });
    })
    .catch(e => {
      response
        .status(HttpStatus.FORBIDDEN)
        .json({
          message: 'Ocurrió un error, revise e intente nuevamente',
          status: false
        })
    });
  }

@Get()
obtenerTodos(@Query('page') pagina, @Res() response){
  return this.registroNotasService.registrosSinAsignar(pagina)
  .then((result) => {
    response.status(HttpStatus.OK).json({
      message: 'Registros obtenidos correctamente',
          result,
          status:true
    });
  })
  .catch(e => {
    response
      .status(HttpStatus.FORBIDDEN)
      .json({
        message: 'Ocurrio un error, revise o intente nuevamente',
        status: false
      })
  });
}


@Post()
@UsePipes(new ValidationPipe())
create(@Body() body: CreateRegistroDto, @Res() response){
  return this.registroNotasService.create(body)
    .then((result) => {
      if(result !== HttpStatus.FORBIDDEN){
        response.status(HttpStatus.OK).json({
          mensaje: 'El registro fue almacenado correctamente.',
          status: true
        });
      } else {
        response
          .json({
              message: 'ATENCION: Ya existe un registro con ese numero',
          })
      }
    })
    .catch(e => {
      response
        .status(HttpStatus.FORBIDDEN)
        .json({
              message: 'Ocurrió un error, revise e intente nuevamente',
              status: false
        })
    });
}

@Put(':id')
@UsePipes(new ValidationPipe())
update(@Param('id') id: number, @Body() body: CreateRegistroDto, @Res() response){
  return this.registroNotasService.update(id, body)
    .then((result) => {
      if (result.filasAfectadas > 0){
        response.status(HttpStatus.OK).json({
          message: 'El registro fue edicato correctamente.',
          result: result.registro,
          status: true
        });
      }else{
        response.status(HttpStatus.OK).json({
          message: 'Error: El registro no existe.', status: false 
        });
      }
    })
    .catch(e => {
      response
          .status(HttpStatus.FORBIDDEN)
          .json({
            message: 'Ocurrio un error',
            status: false
          })
    });
  }
}
