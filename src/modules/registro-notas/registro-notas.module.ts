import { Module } from '@nestjs/common';
import { RegistroNotasService } from './registro-notas.service';
import { RegistroNotasController } from './registro-notas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegistroNota } from './entities/registro-nota.entity';
import { RegistroNotaRepository } from './entities/registro-nota.repository';


@Module({
  imports: [
    TypeOrmModule.forFeature([RegistroNota, RegistroNotaRepository]),
    
  ],
  controllers: [RegistroNotasController],
  providers: [RegistroNotasService],
})
export class RegistroNotasModule {}
