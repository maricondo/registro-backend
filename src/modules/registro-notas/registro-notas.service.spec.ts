import { Test, TestingModule } from '@nestjs/testing';
import { RegistroNotasService } from './registro-notas.service';

describe('RegistroNotasService', () => {
  let service: RegistroNotasService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RegistroNotasService],
    }).compile();

    service = module.get<RegistroNotasService>(RegistroNotasService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
