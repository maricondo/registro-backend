import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRegistroDto } from './dto/create-registro.dto';
import { RegistroNota } from './entities/registro-nota.entity';
import { RegistroNotaRepository } from './entities/registro-nota.repository';

@Injectable()
export class RegistroNotasService {
  private readonly logger = new Logger(RegistroNotasService.name);

  constructor(
    private registroNotasRepository:RegistroNotaRepository,
    /*
    @InjectRepository(RegistroNota)
    private registroNotasRepository: Repository<RegistroNota>,*/
  ){}

  async getAll(pagina){
    if(pagina){
      const take = 10;
      const page = pagina;
      const [registrosNotas, total] = await this.registroNotasRepository.findAndCount({
        select : ["id", 
          "numeroRegistro",
          "fechaIngreso",
          "remitente",
          "descripcion",
          "numeroFojas",
          "tipoDocumento"],
          order: {id:"DESC"},
          take, skip:(page -1) * take
      });
      return {
        data: registrosNotas, meta: {total, page, last_page: Math.ceil(total / take)}
      }
    } else {
      return await this.registroNotasRepository.find({
        select: ["numeroRegistro", 
                        "id", 
                        "fechaIngreso", 
                        "remitente", 
                        "descripcion",
                      "numeroFojas",
                    "tipoDocumento"],
          order: {id: "DESC"}
      });
    }
  }

  async obtenerTodos(pagina){
    if(pagina){
      const take = 10;
      const page = pagina;
      const query = await this.registroNotasRepository
      .createQueryBuilder('registroNota')
      .innerJoinAndSelect('registroNota.asignaciones', 'asignaciones')
      .select([
        'registroNota.id',
        'registroNota.numeroRegistro',
        'registroNota.fechaIngreso',
        'registroNota.remitente',
        'registroNota.descripcion',
        'registroNota.numeroFojas',
        'asignaciones.idRegistro',
        'asignaciones.proveido'
      ])
      .take(take)
      .skip((page-1)*take)
      .orderBy('registroNota.id', 'DESC');
      const result = await query.getMany();
      const total = await query.getCount();
      const totalPaginas = Math.ceil(total/Number(take));
      return {
        data: result, meta: {total, page, totalPaginas}
      }
    }else{
      console.log("entra por aqui")
      return await this.registroNotasRepository
      .createQueryBuilder('registroNota')
      .innerJoinAndSelect('registroNota.asignaciones', 'asignarRegistro')
      .select([
        'registroNota.id',
        'registroNota.numeroRegistro',
        'registroNota.fechaIngreso',
        'registroNota.remitente',
        'registroNota.descripcion',
        'registroNota.numeroFojas',
        'asignarRegistro.idRegistro',
        'asignarRegistro.proveido'
      ])
      .orderBy('registroNota.id', 'DESC')
      .getMany();
    }
  }

  async registrosSinAsignar(pagina){
    if(pagina){
      const take = 10;
      const page = pagina;
      const query = await this.registroNotasRepository
      .createQueryBuilder('registroNota')
      .leftJoinAndSelect('registroNota.asignaciones', 'asignarRegistro')
      .select([
        'registroNota.id',
        'registroNota.numeroRegistro',
        'registroNota.fechaIngreso',
        'registroNota.remitente',
        'registroNota.descripcion',
        'registroNota.numeroFojas',
        'registroNota.idOficina'
      ])
      .where('asignarRegistro.idRegistro is null')
      .take(take)
      .skip((page-1)*take)
      .orderBy('registroNota.id', 'DESC');
      const result = await query.getMany();
      const total = await query.getCount();
      const totalPaginas = Math.ceil(total/Number(take));
      return {
        data: result, meta: {total, page, totalPaginas}
      }
    }else{
      console.log("entra por aqui")
      return await this.registroNotasRepository
      .createQueryBuilder('registroNota')
      .leftJoinAndSelect('registroNota.asignaciones', 'asignarRegistro')
      .select([
        'registroNota.id',
        'registroNota.numeroRegistro',
        'registroNota.fechaIngreso',
        'registroNota.remitente',
        'registroNota.descripcion',
        'registroNota.numeroFojas',
        'registroNota.idOficina'
      ])
      .where('asignarRegistro.idRegistro is null')
      .orderBy('registroNota.id', 'DESC')
      .getMany();
    }
  }  

  async create(body: CreateRegistroDto){
    try {
      const instancia = await this.registroNotasRepository.create(body);
      return this.registroNotasRepository.save(instancia);
    } catch (error) {
      return HttpStatus.FORBIDDEN;
    }
  }

  async update(id:number, body: CreateRegistroDto){
    const instancia = await this.registroNotasRepository.update(id, body);
    return {"filasAfectadas": instancia.affected, "registro": await this.registroNotasRepository.findOne(id)};
  }

  async obtenerRegistros(): Promise<RegistroNota[]>{
    const registros = await this.registroNotasRepository.find();
    return registros;
  }


}
