import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class TipoDocumentoDto {
  @Expose()
  id: number;
  @Expose()
  codigo: number;
  @Expose()
  descripcion: number;
  @Expose()
  abreviatura: number;
  
}
