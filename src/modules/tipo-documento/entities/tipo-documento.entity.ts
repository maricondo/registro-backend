import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({schema:'public', name:'tipo_documentos'})
export class TipoDocumento{
    @PrimaryGeneratedColumn('increment', {name:'tipo_documento_id'})
    id: number;

    @Column({type: 'int4', nullable: false})
    codigo:number;

    @Column({type:'varchar', nullable: false })
    descripcion:string;

    @Column({ type: 'varchar', nullable: true })
    abreviatura: number;

    @Column({ type: 'boolean', name: 'estado_registro', nullable: true })
    estadoRegistro: boolean;
}
