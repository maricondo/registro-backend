import { Controller, Get } from "@nestjs/common";
import { TipoDocumentoService } from "./tipo-documento.service";


@Controller('tipos-documento')
export class TipoDocumentoController {
  constructor(
    private tipoDocumentoService: TipoDocumentoService) {}

  @Get()
  obtenerNotificaciones() {
    return this.tipoDocumentoService.listarTiposDocumentos();
  }
}