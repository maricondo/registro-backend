import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TipoDocumentoController } from "./tipo-documento.controller";
import { TipoDocumentoRepository } from "./tipo-documento.repository";
import { TipoDocumentoService } from "./tipo-documento.service";

@Module({
    imports: [
        ConfigModule,
        TypeOrmModule.forFeature([TipoDocumentoRepository]),
    ],
    controllers: [TipoDocumentoController],
    providers: [TipoDocumentoService],
    exports: [TipoDocumentoService],
})
export class TipoDocumentoModule{}