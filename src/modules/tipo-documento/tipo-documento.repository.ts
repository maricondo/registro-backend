import { EntityRepository, Repository } from "typeorm";
import { TipoDocumento } from "./entities/tipo-documento.entity";


@EntityRepository(TipoDocumento)
export class TipoDocumentoRepository extends Repository<TipoDocumento>{}