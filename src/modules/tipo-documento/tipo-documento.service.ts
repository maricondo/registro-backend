import { Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { plainToClass } from "class-transformer";
import { TipoDocumentoDto } from "./dto/tipo-documento.dto";
import { TipoDocumentoRepository } from "./tipo-documento.repository";

export class TipoDocumentoService {
    private readonly logger = new Logger(TipoDocumentoService.name);
    constructor(
        @InjectRepository(TipoDocumentoRepository)
        private tipoDocumentoRepository:TipoDocumentoRepository
    ){}

    async listarTiposDocumentos(): Promise<RespuestaDto>{
        try{
            const tiposDoc = await this.tipoDocumentoRepository.find();
            const tiposDocDto: Array<TipoDocumentoDto> = tiposDoc?.map(tipo => plainToClass(TipoDocumentoDto, tipo));
            
            let respuesta: RespuestaDto = {
                ok: true,
                mensaje: '',
                mensajeUI: 'La consulta devuelve datos',
                respuesta: tiposDocDto,
              };
              return respuesta;
        } catch (error){
            this.logger.log(error);
            let respuesta: RespuestaDto = {
              ok: false,
              mensaje: '',
              mensajeUI: 'error durante la consulta',
              respuesta: null,
      };
      return respuesta;
    }
  }
}